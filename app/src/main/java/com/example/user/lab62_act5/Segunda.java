package com.example.user.lab62_act5;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class Segunda extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void zoomBack(View button) {
        startActivity(new Intent(this, Principal.class));
        overridePendingTransition(R.animator.zoom_back_in, R.animator.zoom_back_out);
    }

    public void fade(View button) {
        startActivity(new Intent(this, Principal.class));
        overridePendingTransition(R.animator.fade_in, R.animator.fade_out);
    }

    public void right(View button) {
        startActivity(new Intent(this, Principal.class));
        overridePendingTransition(R.animator.right_in, R.animator.right_out);
    }

    public void back(View button) {
        super.onBackPressed();
    }

}
